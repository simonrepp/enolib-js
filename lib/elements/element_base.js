import { errors } from '../errors/validation.js';
import { RANGE_BEGIN } from '../constants.js';

export class ElementBase {
    constructor(context, line, ranges) {
        this._context = context;
        this._line = line;
        this._ranges = ranges;
    }
    
    _untouched() {
        if (!this._touched) return this;
        return false;
    }
    
    /**
    * Constructs and returns a {@link ValidationError} with the supplied message in the context of this element.
    *
    * Note that this only *returns* an error, whether you want to just use its
    * metadata, pass it on or actually throw the error is up to you.
    *
    * @param {string|function(element:Element):string} message A message or a function that receives the element and returns the message.
    * @return {ValidationError} The requested error.
    */
    error(message) {
        return errors.elementError(
            this._context,
            typeof message === 'function' ? message(this) : message,  // TODO: *this* is problematic in this context - what is it?
            this
        );
    }

    isAttribute() {
        return false;
    }
    
    isDocument() {
        return false;
    }
    
    isEmbed() {
        return false;
    }
    
    isField() {
        return false;
    }
    
    isFlag() {
        return false;
    }
    
    isItem() {
        return false;
    }
    
    isSection() {
        return false;
    }
    
    /**
    * Returns the key as string. If a loader is passed, the key is proceessed by it and the result returned.
    * Throws a {@link ValidationError} if an error is intercepted from the loader.
    *
    * @example
    * // Given a field with the key 'foo' ...
    *
    * field.key(key => key.toUpperCase()); // returns 'FOO'
    * field.key(key => { throw 'You shall not pass!'; }); // throws an error based on the intercepted error message
    *
    * @param {function(key:string):any} loader A loader function taking the key as a `string` and returning any other type or throwing a `string` message.
    * @return {any} The result of applying the provided loader to this {@link Element}'s key.
    * @throws {ValidationError} Thrown when an error from the loader is intercepted.
    */
    key(loader = null) {
        if (loader) {
            try {
                return loader(this._key);
            } catch (message) {
                throw errors.keyError(this._context, message, this);
            }
        }

        return this._key;
    }
    
    /**
    * Constructs and returns a {@link ValidationError} with the supplied message in the context of this element's key.
    *
    * Note that this only *returns* an error, whether you want to just use its
    * metadata, pass it on or actually throw the error is up to you.
    *
    * @param {string|function(key:string):string} message A message or a function that receives the element's key and returns the message.
    * @return {ValidationError} The requested error.
    */
    keyError(message) {
        return errors.keyError(
            this._context,
            typeof message === 'function' ? message(this._key) : message,
            this
        );
    }

    /**
    * Returns the key of this {@link Element} as a `string` and touches the element.
    *
    * @return {string} The key of this {@link Element} as a `string`.
    */
    snippet() {
        return new this._context.reporter(this._context).reportElement(this).snippet();
    }
    
    /**
    * Touches this {@link Element}.
    */
    touch() {
        this._touched = true;
    }
}