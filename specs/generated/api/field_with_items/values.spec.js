// THIS FILE IS AUTO-GENERATED (Please submit permanent changes to https://codeberg.org/simonrepp/enolib-common)

import { parse, ParseError, ValidationError } from 'enolib';

describe('Querying existing required values from a field with items', () => {
    it('produces the expected result', () => {
        const input = `field:\n` +
                      `- item\n` +
                      `- item`;
        
        const output = parse(input).field('field').requiredValues();
        
        expect(output).toEqual(['item', 'item']);
    });
});

describe('Querying existing optional string values from a field with items', () => {
    it('produces the expected result', () => {
        const input = `field:\n` +
                      `- item\n` +
                      `- item`;
        
        const output = parse(input).field('field').optionalValues();
        
        expect(output).toEqual(['item', 'item']);
    });
});

describe('Querying missing optional string values from a field with items', () => {
    it('produces the expected result', () => {
        const input = `field:\n` +
                      `-\n` +
                      `-`;
        
        const output = parse(input).field('field').optionalValues();
        
        expect(output).toEqual([null, null]);
    });
});