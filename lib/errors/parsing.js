import { HUMAN_INDEXING, RANGE_BEGIN, RANGE_END } from '../constants.js';
import { cursor, selectLine } from './selections.js';
import { ParseError } from '../error_types.js';

const ATTRIBUTE_OR_FIELD_WITHOUT_KEY = /^\s*([:=]).*$/; // = value OR : value
const EMBED_OR_SECTION_WITHOUT_KEY = /^\s*(--+|#+).*$/; // -- OR #
const ESCAPE_WITHOUT_KEY = /^\s*(`+)(?!`)(\s+)\1.*$/; // ` `
const INVALID_AFTER_ESCAPE = /^\s*(`+)(?!`)(?:(?!\1).)+\1\s*([^=:].*?)\s*$/; // `key` value
const UNTERMINATED_ESCAPED_KEY = /^\s*(`+)(?!`)(.*)$/; // `key

export function invalidLine(context, instruction) {
    const line = context._input.substring(instruction._ranges.line[RANGE_BEGIN], instruction._ranges.line[RANGE_END]);
    
    let match;
    if ( (match = ATTRIBUTE_OR_FIELD_WITHOUT_KEY.exec(line)) ) {
        const operatorColumn = line.indexOf(match[1]);
        const message = match[1] === '=' ? 'attributeWithoutKey' : 'fieldWithoutKey';
        
        return new ParseError(
            context.messages[message](instruction._line + HUMAN_INDEXING),
            new context.reporter(context).reportLine(instruction).snippet(),
            {
                from: cursor(instruction, 'line', RANGE_BEGIN),
                to: { column: operatorColumn, index: instruction._ranges.line[RANGE_BEGIN] + operatorColumn, line: instruction._line }
            }
        );
    } else if ( (match = EMBED_OR_SECTION_WITHOUT_KEY.exec(line)) ) {
        const keyColumn = line.indexOf(match[1]) + match[1].length;
        const message = match[1].startsWith('-') ? 'embedWithoutKey' : 'sectionWithoutKey';
        
        return new ParseError(
            context.messages[message](instruction._line + HUMAN_INDEXING),
            new context.reporter(context).reportLine(instruction).snippet(),
            {
                from: { column: keyColumn, index: instruction._ranges.line[RANGE_BEGIN] + keyColumn, line: instruction._line },
                to: cursor(instruction, 'line', RANGE_END),
            }
        );
    } else if ( (match = ESCAPE_WITHOUT_KEY.exec(line)) ) {
        const gapBeginColumn = line.indexOf(match[1]) + match[1].length;
        const gapEndColumn = line.indexOf(match[1], gapBeginColumn);
        
        return new ParseError(
            context.messages.escapeWithoutKey(instruction._line + HUMAN_INDEXING),
            new context.reporter(context).reportLine(instruction).snippet(),
            {
                from: { column: gapBeginColumn, index: instruction._ranges.line[RANGE_BEGIN] + gapBeginColumn, line: instruction._line },
                to: { column: gapEndColumn, index: instruction._ranges.line[RANGE_BEGIN] + gapEndColumn, line: instruction._line },
            }
        );
    } else if ( (match = INVALID_AFTER_ESCAPE.exec(line)) ) {
        const invalidBeginColumn = line.lastIndexOf(match[2]);
        const invalidEndColumn = invalidBeginColumn + match[2].length;
        
        return new ParseError(
            context.messages.invalidAfterEscape(instruction._line + HUMAN_INDEXING),
            new context.reporter(context).reportLine(instruction).snippet(),
            {
                from: { column: invalidBeginColumn, index: instruction._ranges.line[RANGE_BEGIN] + invalidBeginColumn, line: instruction._line },
                to: { column: invalidEndColumn, index: instruction._ranges.line[RANGE_BEGIN] + invalidEndColumn, line: instruction._line },
            }
        );
    } else if ( (match = UNTERMINATED_ESCAPED_KEY.exec(line)) ) {
        const selectionColumn = line.lastIndexOf(match[2]);
        
        return new ParseError(
            context.messages.unterminatedEscapedKey(instruction._line + HUMAN_INDEXING),
            new context.reporter(context).reportLine(instruction).snippet(),
            {
                from: { column: selectionColumn, index: instruction._ranges.line[RANGE_BEGIN] + selectionColumn, line: instruction._line },
                to: cursor(instruction, 'line', RANGE_END)
            }
        );
    }
}

export function instructionOutsideField(context, instruction, type) {
    return new ParseError(
        context.messages[`${type}OutsideField`](instruction._line + HUMAN_INDEXING),
        new context.reporter(context).reportLine(instruction).snippet(),
        selectLine(instruction)
    );
}

export function mixedFieldContent(context, field, conflicting) {
    return new ParseError(
        context.messages.mixedFieldContent(field._line + HUMAN_INDEXING),
        new context.reporter(context).indicateElement(field).reportLine(conflicting).snippet(),
        selectLine(conflicting)
    );
}

export function sectionLevelSkip(context, section, superSection) {
    const reporter = new context.reporter(context).reportLine(section);
    
    if (!superSection.isDocument()) {
        reporter.indicateLine(superSection);
    }
    
    return new ParseError(
        context.messages.sectionLevelSkip(section._line + HUMAN_INDEXING),
        reporter.snippet(),
        selectLine(section)
    );
}

export function unterminatedEmbed(context, embedBeginInstruction, key) {
    return new ParseError(
        context.messages.unterminatedEmbed(key, embedBeginInstruction._line + HUMAN_INDEXING),
        new context.reporter(context).reportUnterminatedEmbed(embedBeginInstruction).snippet(),
        selectLine(embedBeginInstruction)
    );
}
