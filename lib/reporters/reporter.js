export const DISPLAY = Symbol('Display Line');
export const EMPHASIZE = Symbol('Emphasize Line');
export const INDICATE = Symbol('Indicate Line');
export const OMISSION = Symbol('Insert Omission');
export const QUESTION = Symbol('Question Line');

export class Reporter {
    constructor(context) {
        this._context = context;
        this._index = new Array(this._context._lineCount);
        this._snippet = new Array(this._context._lineCount);
        
        this._buildIndex()
    }
    
    _buildIndex() {
        const traverse = section => {
            for (const element of section._elements) {
                this._index[element._line] = element;
                
                if (element.isSection()) {
                    traverse(element);
                } else if (element.isField()) {
                    if (element.hasItems()) {
                        for (const item of element._elements) {
                            this._index[item._line] = item;
                        }
                    } else if (element.hasAttributes()) {
                        for (const attribute of element._elements) {
                            this._index[attribute._line] = attribute;
                        }
                    }
                } else if (element.isEmbed()) {
                    this._index[element._terminator._line] = element._terminator;
                    for (const line of element._lines) {
                        this._index[line._line] = line;
                    }
                }
            }
        }

        traverse(this._context._document);
        
        for (const meta of this._context._meta) {
            this._index[meta._line] = meta;
        }
    }
    
    _tagAttributesOrItems(element, tag) {
        let scanLine = element._line + 1;
        
        if (element._elements.length === 0)
            return scanLine;

        for (const attributeOrItem of element._elements) {
            while (scanLine < attributeOrItem._line) {
                this._snippet[scanLine] = tag;
                scanLine++;
            }
            
            this._snippet[attributeOrItem._line] = tag;
            
            scanLine = attributeOrItem._line + 1;
        }
        
        return scanLine;
    }
    
    _tagChildren(element, tag) {
        if (element.isField()) {
            if (element.hasAttributes() || element.hasItems()) {
                return this._tagAttributesOrItems(element, tag);
            }

            return element._line + 1;
        } else if (element.isAttribute() || element.isItem()) {
            return element._line + 1;
        } else if (element.isEmbed()) {
            for (const line of element._lines) {
                this._snippet[line._line] = tag;
            }
            
            if (Object.hasOwn(element, '_terminator')) {
                this._snippet[element._terminator._line] = tag;
                return element._terminator._line + 1;
            } else if (element._lines.length > 0) {
                return element._lines[element._lines.length - 1]._line + 1;
            } else {
                return element._line + 1;
            }
        } else if (element.isSection()) {
            return this._tagSection(element, tag);
        }
    }
    
    _tagSection(section, tag, recursive = true) {
        let scanLine = section._line + 1;
        
        for (const element of section._elements) {
            while (scanLine < element._line) {
                this._snippet[scanLine] = tag;
                scanLine++;
            }
            
            if (!recursive && element.isSection()) break;
            
            this._snippet[element._line] = tag;
            
            scanLine = this._tagChildren(element, tag);
        }
        
        return scanLine;
    }
    
    indicateElement(element) {
        this._snippet[element._line] = INDICATE;
        this._tagChildren(element, INDICATE);
        
        return this;
    }
    
    indicateLine(element) {
        this._snippet[element._line] = INDICATE;
        return this;
    }
    
    questionLine(element) {
        this._snippet[element._line] = QUESTION;
        return this;
    }
    
    reportElement(element) {
        this._snippet[element._line] = EMPHASIZE;
        this._tagChildren(element, INDICATE);
        
        return this;
    }
    
    reportElements(elements) {
        for (const element of elements) {
            this._snippet[element._line] = EMPHASIZE;
            this._tagChildren(element, INDICATE);
        }
        
        return this;
    }
    
    reportEmbedValue(element) {
        for (const line of element._lines) {
            this._snippet[line._line] = EMPHASIZE;
        }
        
        return this;
    }
    
    reportLine(instruction) {
        this._snippet[instruction._line] = EMPHASIZE;
        
        return this;
    }
    
    reportMissingElement(parent) {
        if (!(parent.isDocument())) {
            this._snippet[parent._line] = INDICATE;
        }
        
        if (parent.isSection()) {
            this._tagSection(parent, QUESTION, false);
        } else {
            this._tagChildren(parent, QUESTION);
        }
        
        return this;
    }

    reportUnterminatedEmbed(embedBeginInstruction) {
        this._snippet[embedBeginInstruction._line] = EMPHASIZE;

        for (let line = embedBeginInstruction._line + 1; line < this._context._lineCount; line++) {
            this._snippet[line] = INDICATE;
        }

        return this;
    }
    
    snippet() {
        if (this._snippet.every(line => line === undefined)) {
            for (let line = 0; line < this._snippet.length; line++) {
                this._snippet[line] = QUESTION;
            }
        } else {
            // TODO: Possibly better algorithm for this
            
            for (const [line, tag] of this._snippet.entries()) {
                if (tag !== undefined) continue;
                
                // TODO: Prevent out of bounds access
                if (this._snippet[line + 2] !== undefined && this._snippet[line + 2] !== DISPLAY ||
                    this._snippet[line - 2] !== undefined && this._snippet[line - 2] !== DISPLAY ||
                    this._snippet[line + 1] !== undefined && this._snippet[line + 1] !== DISPLAY ||
                    this._snippet[line - 1] !== undefined && this._snippet[line - 1] !== DISPLAY) {
                    this._snippet[line] = DISPLAY;
                } else if (this._snippet[line + 3] !== undefined && this._snippet[line + 3] !== DISPLAY) {
                    this._snippet[line] = OMISSION;
                }
            }
            
            if (this._snippet[this._snippet.length - 1] === undefined) {
                this._snippet[this._snippet.length - 1] = OMISSION;
            }
        }
        
        return this._print();
    }
}
