// THIS FILE IS AUTO-GENERATED (Please submit permanent changes to https://codeberg.org/simonrepp/enolib-common)

import { parse, ParseError, ValidationError } from 'enolib';

describe('Querying all elements from a section', () => {
    it('produces the expected result', () => {
        const input = `# section\n` +
                      `one: value\n` +
                      `two: value`;
        
        const output = parse(input).section('section').elements().map(element => element.key());
        
        expect(output).toEqual(['one', 'two']);
    });
});

describe('Querying elements from a section by key', () => {
    it('produces the expected result', () => {
        const input = `# section\n` +
                      `field: value\n` +
                      `other: one\n` +
                      `other: two`;
        
        const output = parse(input).section('section').elements('other').map(element => element.requiredValue());
        
        expect(output).toEqual(['one', 'two']);
    });
});