export class MissingElementBase {
    constructor(key, parent) {
        this._key = key;
        this._parent = parent;
    }
    
    _missingError(_element) {
        this._parent._missingError(this);
    }
    
    key(_loader) {
        this._parent._missingError(this);
    }

    touch() {
        this._parent._missingError(this);
    }
}
