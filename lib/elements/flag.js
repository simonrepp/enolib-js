import { ElementBase } from './element_base.js';

export class Flag extends ElementBase {
    constructor(
        context,
        key,
        line,
        parent,
        ranges
    ) {
        super(context, line, ranges);

        this._key = key;
        this._parent = parent;
        this._touched = false;
    }

    get [Symbol.toStringTag]() {
        return 'Flag';
    }

    isFlag() {
        return true;
    }
    
    // TODO: Removable? (DRY, here & elsewhere)
    parent() {
        return this._parent;
    }
    
    /**
    * Returns a debug representation of this {@link Flag} in the form of `[object Flag key=foo]`.
    *
    * @return {string} A debug representation of this {@link Flag}.
    */
    toString() {
        return `[object Flag key=${this._key}]`;
    }
}
