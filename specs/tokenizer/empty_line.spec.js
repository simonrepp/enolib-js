import { unpackInternal } from './util.js';

let input = '\n' +
            ' \n' +
            '  \n' +
            '   \n' +
            '\n' +
            ' \n' +
            '  \n' +
            '   \n';

describe('Empty line tokenization', () => {
    it('performs as specified', () => {
        expect(unpackInternal(input)).toMatchSnapshot();
    });
    
    describe('Zero-length input', () => {
        it('performs as specified', () => {
            expect(unpackInternal('')).toMatchSnapshot();
        });
    })
});
