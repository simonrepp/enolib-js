import { Context } from './context.js';
import { RANGE_BEGIN, RANGE_END } from './constants.js';

const checkInDocumentByIndex = (document, index) => {
    return checkInSectionByIndex(document, index);
};

const checkInDocumentByLine = (document, line) => {
    return checkInSectionByLine(document, line);
};

const checkEmbedByIndex = (embed, index) => {
    if (index < embed._ranges.line[RANGE_BEGIN] || index > embed._terminator._ranges.line[RANGE_END])
        return false;
    
    if (index <= embed._ranges.line[RANGE_END])
        return { element: embed, instruction: embed };
    
    if (index >= embed._terminator._ranges.line[RANGE_BEGIN])
        return { element: embed, instruction: embed._terminator };
    
    return { element: embed, instruction: embed._lines.find(line => index <= line._ranges.line[RANGE_END]) };
};

const checkEmbedByLine = (embed, line) => {
    if (line < embed._line || line > embed._terminator._line)
        return false;
    
    if (line === embed._line)
        return { element: embed, instruction: embed };
    
    if (line === embed._terminator._line)
        return { element: embed, instruction: embed._terminator };
    
    return { element: embed, instruction: embed._lines.find(valueLine => valueLine._line === line) };
};

const checkFieldByIndex = (field, index) => {
    if (index < field._ranges.line[RANGE_BEGIN])
        return false;
    
    if (index <= field._ranges.line[RANGE_END])
        return { element: field, instruction: field };
    
    if (field.hasAttributes()) {
        if (index > field._elements[field._elements.length - 1]._ranges.line[RANGE_END])
            return false;
        
        for (const attribute of field._elements) {
            if (index < attribute._ranges.line[RANGE_BEGIN])
                return { element: field, instruction: null };
            
            if (index <= attribute._ranges.line[RANGE_END])
                return { element: attribute, instruction: attribute };
        }
    } else if (field.hasItems()) {
        if (index > field._elements[field._elements.length - 1]._ranges.line[RANGE_END])
            return false;
        
        for (const item of field._elements) {
            if (index < item._ranges.line[RANGE_BEGIN]) {
                return { element: field, instruction: null };
            }
            
            if (index <= item._ranges.line[RANGE_END])
                return { element: item, instruction: item };
        }
    }
    
    return false;
};

const checkFieldByLine = (field, line) => {
    if (line < field._line)
        return false;
    
    if (line === field._line)
        return { element: field, instruction: field };

    if (field.hasAttributes()) {
        if (line > field._elements[field._elements.length - 1]._line)
            return false;
        
        for (const attribute of field._elements) {
            if (line === attribute._line)
                return { element: attribute, instruction: attribute };
            
            if (line < attribute._line) {
                return { element: field, instruction: null };
            }
        }
    } else if (field.hasItems()) {
        if (line > field._elements[field._elements.length - 1]._line)
            return false;
        
        for (const item of field._elements) {
            if (line === item._line)
                return { element: item, instruction: item };
            
            if (line < item._line) {
                return { element: field, instruction: null };
            }
        }
    }
    
    return false;
};

const checkInSectionByIndex = (section, index) => {
    for (let elementIndex = section._elements.length - 1; elementIndex >= 0; elementIndex--) {
        const element = section._elements[elementIndex];
        
        if (index < element._ranges.line[RANGE_BEGIN])
            continue;
        
        if (index <= element._ranges.line[RANGE_END])
            return { element: element, instruction: element };
        
        if (element.isField()) {
            const matchInField = checkFieldByIndex(element, index);
            if (matchInField) return matchInField;
        } else if (element.isEmbed()) {
            const matchInEmbed = checkEmbedByIndex(element, index);
            if (matchInEmbed) return matchInEmbed;
        } else if (element.isSection()) {
            return checkInSectionByIndex(element, index);
        }
        
        break;
    }
    return { element: section, instruction: null };
};


const checkInSectionByLine = (section, line) => {
    for (let elementIndex = section._elements.length - 1; elementIndex >= 0; elementIndex--) {
        const element = section._elements[elementIndex];
        
        if (element._line > line)
            continue;

        if (element._line === line)
            return { element: element, instruction: element };
        
        if (element.isField()) {
            const matchInField = checkFieldByLine(element, line);
            if (matchInField) return matchInField;
        } else if (element.isEmbed()) {
            const matchInEmbed = checkEmbedByLine(element, line);
            if (matchInEmbed) return matchInEmbed;
        } else if (element.isSection()) {
            return checkInSectionByLine(element, line);
        }
                
        break;
    }
    return { element: section, instruction: null };
};

export function lookup(position, context) {
    let { column, index, line } = position;
    
    let match;
    if (index === undefined) {
        if (line < 0 || line >= context._lineCount)
            throw new RangeError(`You are trying to look up a line (${line}) outside of the document's line range (0-${context._lineCount - 1})`);
        
        match = checkInDocumentByLine(context._document, line);
    } else {
        if (index < 0 || index > context._input.length)
            throw new RangeError(`You are trying to look up an index (${index}) outside of the document's index range (0-${context._input.length})`);
        
        match = checkInDocumentByIndex(context._document, index);
    }

    const result = {
        element: match.element,
        range: null
    };
    
    let instruction = match.instruction;
    
    if (!instruction) {
        if (index === undefined) {
            instruction = context._meta.find(instruction => instruction._line === line);
        } else {
            instruction = context._meta.find(instruction =>
                index >= instruction._ranges.line[RANGE_BEGIN] && index <= instruction._ranges.line[RANGE_END]
            );
        }
        
        if (!instruction)
            return result;
    }
    
    let rightmostMatch = instruction._ranges.line[RANGE_BEGIN];
    
    if (index === undefined) {
        index = instruction._ranges.line[RANGE_BEGIN] + column;
    }
    
    for (const [type, range] of Object.entries(instruction._ranges)) {
        if (type === 'line') continue;
        
        if (index >= range[RANGE_BEGIN] && index <= range[RANGE_END] && range[RANGE_BEGIN] >= rightmostMatch) {
            result.range = type;
            // TODO: Provide content of range too as convenience
            rightmostMatch = index;
        }
    }
    
    return result;
}
