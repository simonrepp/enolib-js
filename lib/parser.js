import { Attribute } from './elements/attribute.js';
import { Embed } from './elements/embed.js';
import { Field } from './elements/field.js';
import { Flag } from './elements/flag.js';
import { Item } from './elements/item.js';
import { Section } from './elements/section.js';

import {
    invalidLine,
    instructionOutsideField,
    mixedFieldContent,
    sectionLevelSkip,
    unterminatedEmbed
} from './errors/parsing.js';

import {
    ATTRIBUTE_OPERATOR_INDEX,
    ATTRIBUTE_VALUE_INDEX,
    COMMENT_INDEX,
    COMMENT_OPERATOR_INDEX,
    EMBED_KEY_INDEX,
    EMBED_OPERATOR_INDEX,
    EMPTY_LINE_INDEX,
    FIELD_OPERATOR_INDEX,
    FIELD_VALUE_INDEX,
    GRAMMAR_REGEXP,
    ITEM_OPERATOR_INDEX,
    ITEM_VALUE_INDEX,
    KEY_ESCAPE_BEGIN_OPERATOR_INDEX,
    KEY_ESCAPED_INDEX,
    KEY_UNESCAPED_INDEX,
    SECTION_KEY_INDEX,
    SECTION_OPERATOR_INDEX
} from './grammar_matcher.js';

export class Parser {
    constructor(context) {
        this._context = context;
        this._index = 0;
        this._line = 0;
    }

    parse() {
        if (this._context._input.length === 0) {
            this._context._lineCount = 1;
            return this._context._document;
        }
        
        let lastField = null;
        let lastSection = this._context._document;
        let lastSectionDepth = 0;
        let unappliedSpacing = false;
        
        // TODO: This is maybe not really (concurrency) safe (?)
        // GRAMMAR_REGEXP is "compile time" instantiated as one single instance shared among all concurrent "threads" ...
        const matcherRegex = GRAMMAR_REGEXP;
        matcherRegex.lastIndex = this._index;

        while (this._index < this._context._input.length) {
            const match = matcherRegex.exec(this._context._input);
            
            if (match === null) {
                const instruction = this.parseAfterError();
                throw invalidLine(this._context, instruction);
            }
            
            if (match[EMPTY_LINE_INDEX] === undefined) {
                const ranges = { line: [this._index, matcherRegex.lastIndex] };
                
                if (match[FIELD_OPERATOR_INDEX] !== undefined) {
                    let key = match[KEY_UNESCAPED_INDEX];
                    const value = match[FIELD_VALUE_INDEX];
                    
                    let fieldOperatorIndex;
                    if (key !== undefined) {
                        const keyIndex = this._context._input.indexOf(key, this._index);
                        fieldOperatorIndex = this._context._input.indexOf(':', keyIndex + key.length);
                        
                        ranges.fieldOperator = [fieldOperatorIndex, fieldOperatorIndex + 1];
                        ranges.key = [keyIndex, keyIndex + key.length];
                    } else {
                        key = match[KEY_ESCAPED_INDEX];
                        
                        const escapeOperator = match[KEY_ESCAPE_BEGIN_OPERATOR_INDEX];
                        const escapeBeginOperatorIndex = this._context._input.indexOf(escapeOperator, this._index);
                        const keyIndex = this._context._input.indexOf(key, escapeBeginOperatorIndex + escapeOperator.length);
                        const escapeEndOperatorIndex = this._context._input.indexOf(escapeOperator, keyIndex + key.length);
                        fieldOperatorIndex = this._context._input.indexOf(':', escapeEndOperatorIndex + escapeOperator.length);
                        
                        ranges.escapeBeginOperator = [escapeBeginOperatorIndex, escapeBeginOperatorIndex + escapeOperator.length];
                        ranges.escapeEndOperator = [escapeEndOperatorIndex, escapeEndOperatorIndex + escapeOperator.length];
                        ranges.fieldOperator = [fieldOperatorIndex, fieldOperatorIndex + 1];
                        ranges.key = [keyIndex, keyIndex + key.length];
                    }

                    if (value !== undefined) {
                        const valueIndex = this._context._input.indexOf(value, fieldOperatorIndex + 1);
                        ranges.value = [valueIndex, valueIndex + value.length];
                    }

                    const field = new Field(
                        this._context,
                        key,
                        this._line,
                        lastSection,
                        ranges,
                        value ?? null
                    );
                    
                    lastSection._elements.push(field);
                    lastField = field;
                } else if (match[ITEM_OPERATOR_INDEX] !== undefined) {
                    const operatorIndex = this._context._input.indexOf('-', this._index);
                    
                    ranges.itemOperator = [operatorIndex, operatorIndex + 1];
                    
                    const value = match[ITEM_VALUE_INDEX];

                    if (value !== undefined) {
                        const valueIndex = this._context._input.indexOf(value, operatorIndex + 1);
                        ranges.value = [valueIndex, valueIndex + value.length];
                    }

                    const item = new Item(
                        this._context,
                        this._line,
                        lastField,
                        ranges,
                        value ?? null
                    );
                    
                    if (lastField === null) {
                        const instruction = this.parseAfterError();
                        throw instructionOutsideField(this._context, instruction, 'item');
                    } else if (!lastField._appendItem(item)) {
                        const instruction = this.parseAfterError();
                        throw mixedFieldContent(this._context, lastField, instruction);
                    }
                } else if (match[ATTRIBUTE_OPERATOR_INDEX] !== undefined) {
                    let key = match[KEY_UNESCAPED_INDEX];
                    const value = match[ATTRIBUTE_VALUE_INDEX];
                    
                    let attributeOperatorIndex;
                    if (key === undefined) {
                        key = match[KEY_ESCAPED_INDEX];

                        const escapeOperator = match[KEY_ESCAPE_BEGIN_OPERATOR_INDEX];
                        const escapeBeginOperatorIndex = this._context._input.indexOf(escapeOperator, this._index);
                        const keyIndex = this._context._input.indexOf(key, escapeBeginOperatorIndex + escapeOperator.length);
                        const escapeEndOperatorIndex = this._context._input.indexOf(escapeOperator, keyIndex + key.length);
                        attributeOperatorIndex = this._context._input.indexOf('=', escapeEndOperatorIndex + escapeOperator.length);
                        
                        ranges.escapeBeginOperator = [escapeBeginOperatorIndex, escapeBeginOperatorIndex + escapeOperator.length];
                        ranges.escapeEndOperator = [escapeEndOperatorIndex, escapeEndOperatorIndex + escapeOperator.length];
                        ranges.attributeOperator = [attributeOperatorIndex, attributeOperatorIndex + 1];
                        ranges.key = [keyIndex, keyIndex + key.length];
                    } else {
                        const keyIndex = this._context._input.indexOf(key, this._index);
                        attributeOperatorIndex = this._context._input.indexOf('=', keyIndex + key.length);
                        
                        ranges.attributeOperator = [attributeOperatorIndex, attributeOperatorIndex + 1];
                        ranges.key = [keyIndex, keyIndex + key.length];
                    }

                    if (value !== undefined) {
                        const valueIndex = this._context._input.indexOf(value, attributeOperatorIndex + 1);
                        ranges.value = [valueIndex, valueIndex + value.length];
                    }

                    const attribute = new Attribute(
                        this._context,
                        key,
                        this._line,
                        lastField,
                        ranges,
                        value ?? null
                    );
                    
                    if (lastField === null) {
                        const instruction = this.parseAfterError();
                        throw instructionOutsideField(this._context, instruction, 'attribute');
                    } else if (!lastField._appendAttribute(attribute)) {
                        const instruction = this.parseAfterError();
                        throw mixedFieldContent(this._context, lastField, instruction);
                    }
                } else if (match[SECTION_OPERATOR_INDEX] !== undefined) {
                    const sectionOperator = match[SECTION_OPERATOR_INDEX];
                    
                    const sectionDepth = sectionOperator.length;
                    
                    const sectionOperatorIndex = this._context._input.indexOf(sectionOperator, this._index);
                    const key = match[SECTION_KEY_INDEX];
                    
                    const keyIndex = this._context._input.indexOf(key, sectionOperatorIndex + sectionOperator.length);
                    
                    ranges.key = [keyIndex, keyIndex + key.length];
                    ranges.sectionOperator = [sectionOperatorIndex, sectionOperatorIndex + sectionOperator.length];
                    
                    let parent;
                    if (sectionDepth === lastSectionDepth + 1) {
                        parent = lastSection;
                    } else if (sectionDepth === lastSectionDepth) {
                        parent = lastSection._parent;
                    } else if (sectionDepth < lastSectionDepth) {
                        while (sectionDepth < lastSectionDepth) {
                            lastSection = lastSection._parent;
                            lastSectionDepth--;
                        }
                        
                        parent = lastSection._parent;
                    } else {
                        const instruction = this.parseAfterError();
                        throw sectionLevelSkip(this._context, instruction, lastSection);
                    }

                    lastSectionDepth = sectionDepth;

                    const section = new Section(
                        this._context,
                        key,
                        this._line,
                        parent,
                        ranges
                    );

                    section._parent._elements.push(section);
                    
                    lastSection = section;
                    lastField = null;
                } else if (match[EMBED_OPERATOR_INDEX] !== undefined) {
                    const operator = match[EMBED_OPERATOR_INDEX];
                    
                    const key = match[EMBED_KEY_INDEX];
                    const lines = [];
                    
                    let operatorIndex = this._context._input.indexOf(operator, this._index);
                    let keyIndex = this._context._input.indexOf(key, operatorIndex + operator.length);
                    
                    ranges.embedOperator = [operatorIndex, operatorIndex + operator.length];
                    ranges.key = [keyIndex, keyIndex + key.length];

                    const beginIndex = this._index;
                    const beginLine = this._line;
                    
                    this._index = matcherRegex.lastIndex + 1;
                    this._line += 1;
                    
                    const keyEscaped = key.replace(/[-/\\^$*+?.()|[\]{}]/g, '\\$&');
                    const terminatorMatcher = new RegExp(`[^\\S\\n]*(${operator})(?!-)[^\\S\\n]*(${keyEscaped})[^\\S\\n]*(?=\\n|$)`, 'y');
                    
                    while (true) {
                        terminatorMatcher.lastIndex = this._index;
                        let terminatorMatch = terminatorMatcher.exec(this._context._input);
                        
                        if (terminatorMatch) {
                            operatorIndex = this._context._input.indexOf(operator, this._index);
                            keyIndex = this._context._input.indexOf(key, operatorIndex + operator.length);
                            
                            const terminator = {
                                _line: this._line,
                                _ranges: {
                                    line: [this._index, terminatorMatcher.lastIndex],
                                    embedOperator: [operatorIndex, operatorIndex + operator.length],
                                    key: [keyIndex, keyIndex + key.length]
                                }
                            };

                            const embed = new Embed(
                                this._context,
                                key,
                                beginLine,
                                lines,
                                lastSection,
                                ranges,
                                terminator
                            );

                            lastSection._elements.push(embed);
                            lastField = null;   

                            matcherRegex.lastIndex = terminatorMatcher.lastIndex;
                            
                            break;
                        } else {
                            const endofLineIndex = this._context._input.indexOf('\n', this._index);
                            
                            if (endofLineIndex === -1) {
                                // TODO: Reporting should still be improved here, so that the embed begin
                                //       line is highlighted properly as operator + key, and the following
                                //       lines properly as value. Right now it just treats everything as
                                //       unparsed.
                                this._index = beginIndex;
                                this._line = beginLine;
                                const instruction = this.parseAfterError();
                                throw unterminatedEmbed(this._context, instruction, key);
                            } else {
                                lines.push({
                                    _line: this._line,
                                    _ranges: {
                                        line: [this._index, endofLineIndex],
                                        value: [this._index, endofLineIndex]  // TODO: line range === value range, drop value range? (see how the custom terminal reporter eg. handles this for syntax coloring, then revisit)
                                    }
                                });
                                this._index = endofLineIndex + 1;
                                this._line++;
                            }
                        }
                    }
                } else if (match[COMMENT_OPERATOR_INDEX] !== undefined) {
                    const operatorIndex = this._context._input.indexOf('>', this._index);
                    ranges.commentOperator = [operatorIndex, operatorIndex + 1];
                    
                    const value = match[COMMENT_INDEX];
                    if (value !== undefined) {
                        const commentIndex = this._context._input.indexOf(value, operatorIndex + 1);
                        ranges.comment = [commentIndex, commentIndex + value.length];
                    }

                    const comment = {
                        _comment: value ?? null,
                        _line: this._line,
                        _ranges: ranges
                    };
                    
                    this._context._meta.push(comment);
                } else if (match[KEY_UNESCAPED_INDEX] !== undefined) {
                    const key = match[KEY_UNESCAPED_INDEX];
                    
                    const keyIndex = this._context._input.indexOf(key, this._index);
                    
                    ranges.key = [keyIndex, keyIndex + key.length];

                    const flag = new Flag(
                        this._context,
                        key,
                        this._line,
                        lastSection,
                        ranges
                    );
                    
                    lastSection._elements.push(flag);
                    lastField = null;
                } else /* if (match[KEY_ESCAPED_INDEX] !== undefined) */ {
                    const key = match[KEY_ESCAPED_INDEX];

                    const escapeOperator = match[KEY_ESCAPE_BEGIN_OPERATOR_INDEX];
                    const escapeBeginOperatorIndex = this._context._input.indexOf(escapeOperator, this._index);
                    const keyIndex = this._context._input.indexOf(key, escapeBeginOperatorIndex + escapeOperator.length);
                    const escapeEndOperatorIndex = this._context._input.indexOf(escapeOperator, keyIndex + key.length);
                    
                    ranges.escapeBeginOperator = [escapeBeginOperatorIndex, escapeBeginOperatorIndex + escapeOperator.length];
                    ranges.escapeEndOperator = [escapeEndOperatorIndex, escapeEndOperatorIndex + escapeOperator.length];
                    ranges.key = [keyIndex, keyIndex + key.length];

                    const flag = new Flag(
                        this._context,
                        key,
                        this._line,
                        lastSection,
                        ranges
                    );
                    
                    lastSection._elements.push(flag);
                    lastField = null;
                }
            }
            
            this._line += 1;
            this._index = matcherRegex.lastIndex + 1;
            matcherRegex.lastIndex = this._index;
        } // ends while (this._index < this._context._input.length) {

        this._context._lineCount = this._context._input[this._context._input.length - 1] === '\n' ? this._line + 1 : this._line;

        return this._context._document;
    }

    parseAfterError() {
        let errorInstruction;
        while (this._index < this._context._input.length) {
            let endOfLineIndex = this._context._input.indexOf('\n', this._index);
            
            if (endOfLineIndex === -1) {
                endOfLineIndex = this._context._input.length;
            }
            
            const instruction = {
                _line: this._line,
                _ranges: { line: [this._index, endOfLineIndex] },
                _unparsed: true
            };
            
            errorInstruction ||= instruction;
            
            this._context._meta.push(instruction);
            this._index = endOfLineIndex + 1;
            this._line++;
        }
        
        this._context._lineCount = this._context._input[this._context._input.length - 1] === '\n' ? this._line + 1 : this._line;
        
        return errorInstruction;
    }
}

