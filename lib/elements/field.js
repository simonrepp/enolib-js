import { errors } from '../errors/validation.js';
import { MissingAttribute } from './missing/missing_attribute.js';
import { ValueElementBase } from './value_element_base.js';

const ALLOWS_ATTRIBUTES = 0x1;
const ALLOWS_ITEMS = 0x2;
const HAS_ATTRIBUTES = 0x4;
const HAS_ITEMS = 0x8;

export class Field extends ValueElementBase {
    constructor(
        context,
        key,
        line,
        parent,
        ranges,
        value = null
    ) {
        super(context, line, ranges);

        this._allAttributesRequired = false;
        this._contentType = value === null ? (ALLOWS_ATTRIBUTES | ALLOWS_ITEMS) : 0;
        this._elements = [];
        this._key = key;
        this._parent = parent;
        this._touched = false;
        this._value = value;
    }
    
    get [Symbol.toStringTag]() {
        return 'Field';
    }

    _appendAttribute(attribute) {
        if (!(this._contentType & ALLOWS_ATTRIBUTES)) return false;
        this._contentType = ALLOWS_ATTRIBUTES | HAS_ATTRIBUTES;
        this._elements.push(attribute);
        return true;
    }

    _appendItem(item) {
        if (!(this._contentType & ALLOWS_ITEMS)) return false;
        this._contentType = ALLOWS_ITEMS | HAS_ITEMS;
        this._elements.push(item);
        return true;
    }

    _attribute(key, required = null) {
        const attributes = this._attributesMap(key);
        
        if (attributes.length === 0) {
            if (required || required === null && this._allAttributesRequired) {
                throw errors.missingElement(this._context, key, this, 'missingAttribute');
            } else if (required === null) {
                return new MissingAttribute(key, this);
            } else {
                return null;
            }
        }
        
        if (attributes.length > 1)
            throw errors.unexpectedMultipleElements(
                this._context,
                key,
                attributes,
                'expectedSingleAttribute'
            );

        attributes[0].touch();
        
        return attributes[0];
    }
    
    _attributesMap(key = null) {
        if (!Object.hasOwn(this, '_attributesMapCache')) {
            this._attributesMapCache = {};
            
            if (this._contentType & HAS_ATTRIBUTES) {
                for (const attribute of this._elements) {
                    if (Object.hasOwn(this._attributesMapCache, attribute._key)) {
                        this._attributesMapCache[attribute._key].push(attribute);
                    } else {
                        this._attributesMapCache[attribute._key] = [attribute];
                    }
                }
            }
        }
        
        if (key === null) return this._attributesMapCache;
        if (Object.hasOwn(this._attributesMapCache, key)) return this._attributesMapCache[key];
        return [];
    }

    _loadValue(loader, required) {
        if (this.hasAttributes() || this.hasItems()) {
            throw errors.unexpectedFieldContent(this._context, null, this, 'expectedValue');
        }
        
        const value = this._getValue();
        
        if (value === null) {
            if (required)
                throw errors.missingValue(this._context, this);
            
            return null;
        }
        
        if (!loader)
            return value;
        
        try {
            return loader(value);
        } catch (message) {
            throw errors.valueError(this._context, message, this);
        }
    }
    
    _missingError(attribute) {
        throw errors.missingElement(this._context, attribute._key, this, 'missingAttribute');
    }
    
    _untouched() {
        if (!this._touched)
            return this;
        
        if (this._contentType & (HAS_ATTRIBUTES | HAS_ITEMS)) {
            const untouchedElement = this._elements.find(element => !element._touched);
            if (untouchedElement) return untouchedElement;
        }
        
        return false;
    }
    
    allAttributesRequired(required = true) {
        this._allAttributesRequired = required;
    }
    
    /**
    * Assert that all attributes inside this field have been touched
    * @param {string} message A static string error message or a message function taking the excess element and returning an error string
    * @param {object} options
    * @param {array} options.except An array of attribute keys to exclude from assertion
    * @param {array} options.only Specifies to ignore all attributes but the ones included in this array of element keys
    */
    assertAllTouched(...optional) {
        let message = null;
        let options = {};
        
        for (const argument of optional) {
            if (typeof argument === 'object') {
                options = argument;
            } else {
                message = argument
            }
        }
        
        for (const [key, attributes] of Object.entries(this._attributesMap())) {
            if (Object.hasOwn(options, 'except') && options.except.includes(key)) continue;
            if (Object.hasOwn(options, 'only') && !options.only.includes(key)) continue;
            
            for (const attribute of attributes) {
                if (!attribute._touched) {
                    if (typeof message === 'function') { message = message(attribute); }
                    throw errors.unexpectedElement(this._context, message, attribute);
                }
            }
        }
    }
    
    /**
    * Returns the attribute with the specified `key`.
    *
    * @param {string} [key] The key of the attribute to return. Can be left out to validate and query a single attribute with an arbitrary key.
    * @return {Field|MissingField} The attribute with the specified key, if available, or a {@link MissingField} proxy instance.
    */
    attribute(key = null) {
        return this._attribute(key);
    }
    
    /**
    * Returns the attributes of this {@link Field} as an array in the original document order.
    *
    * @param {string} [key] If provided only attributes with the specified key are returned.
    * @return {Attribute[]} The attributes of this {@link Field}.
    */
    attributes(key = null) {
        if (!(this._contentType & ALLOWS_ATTRIBUTES))
            throw errors.unexpectedFieldContent(this._context, key, this, 'expectedAttributes');

        const attributes = key === null ? this._elements : this._attributesMap(key);
        for (const attribute of attributes) { attribute.touch() }
        return attributes;
    }

    /**
    * Check whether the field has attributes.
    *
    * @return {bool} Whether the field has attributes.
    */
    hasAttributes() {
        return (this._contentType & HAS_ATTRIBUTES) === HAS_ATTRIBUTES;
    }

    /**
    * Check whether the field has items.
    *
    * @return {bool} Whether the field has items.
    */
    hasItems() {
        return (this._contentType & HAS_ITEMS) === HAS_ITEMS;
    }

    /**
    * Check whether the field has a value.
    *
    * @return {bool} Whether the field has a value.
    */
    hasValue() {
        return this._value !== null;
    }

    isField() {
        return true;
    }
    
    /**
     * Returns the items in this {@link Field} as an array.
     *
     * @return {Item[]} The items in this {@link Field}.
     */
    items() {
        if (!(this._contentType & ALLOWS_ITEMS))
            throw errors.unexpectedFieldContent(this._context, null, this, 'expectedItems');

        for (const item of this._elements) { item.touch() }
        return this._elements;
    }
    
    /**
    * Returns the number of items in this {@link Field} as a `number`.
    *
    * @return {number} The number of items in this {@link Field}.
    */
    length() {
        if (this._contentType & (HAS_ATTRIBUTES | HAS_ITEMS)) return this._elements.length;
        return 0;
    }
    
    optionalAttribute(key = null) {
        // TODO: Error if this field has items/value
        
        return this._attribute(key, false);
    }
    
    /**
    * Passes the value of this {@link Field} through the provided loader, returns the result and touches the element.
    * The loader is only invoked if there is a value, otherwise `null` is returned directly.
    * Throws a {@link ValidationError} if an error is intercepted from the loader.
    *
    * @example
    * // Given a field containing the value 'foo' ...
    *
    * field.optionalValue(value => value.toUpperCase()); // returns 'FOO'
    * field.optionalValue(value => { throw 'You shall not pass!'; }); // throws an error based on the intercepted error message
    *
    * // Given a field containing no value ...
    *
    * field.optionalValue(value => value.toUpperCase()); // returns null
    * field.optionalValue(value => { throw 'You shall not pass!'; }); // returns null
    *
    * @param {function(value:string):any} loader A loader function taking a `string` value and returning any other type or throwing a `string` message.
    * @return {?any} The result of applying the provided loader to this {@link Field}'s value, or `null` when empty.
    * @throws {ValidationError} Thrown when an error from the loader is intercepted.
    */
    optionalValue(loader = null) {
        return this._loadValue(loader, false);
    }
    
    optionalValues(loader = null) {
        // TODO: Error if this field has attributes/value
        
        return this._elements.map(item => {
            item.touch();
            return item.optionalValue(loader);
        });
    }
    
    /**
    * Returns the parent {@link Section}.
    *
    * @return {Section} The parent section.
    */
    parent() {
        return this._parent;
    }
    
    requiredAttribute(key = null) {
        if (!(this._contentType & ALLOWS_ATTRIBUTES))
            throw errors.unexpectedFieldContent(this._context, key, this, 'expectedAttributes');
        
        return this._attribute(key, true);
    }
    
    /**
    * Passes the value of this {@link Field} through the provided loader, returns the result and touches the element.
    * The loader is only invoked if there is a value, otherwise a {@link ValidationError} is thrown directly.
    * Also throws a {@link ValidationError} if an error is intercepted from the loader.
    *
    * @example
    * // Given a field containing the value 'foo' ...
    *
    * field.requiredValue(value => value.toUpperCase()); // returns 'FOO'
    * field.requiredValue(value => { throw 'You shall not pass!'; }); // throws an error based on the intercepted error message
    *
    * // Given a field containing no value ...
    *
    * field.requiredValue(value => value.toUpperCase()); // throws an error stating that a required value is missing
    * field.requiredValue(value => { throw 'You shall not pass!'; }); // throws an error stating that a required value is missing
    *
    * @param {function(value:string):any} loader A loader function taking a `string` value and returning any other type or throwing a `string` message.
    * @return {any} The result of applying the provided loader to this {@link Field}'s value.
    * @throws {ValidationError} Thrown when there is no value or an error from the loader is intercepted.
    */
    requiredValue(loader = null) {
        return this._loadValue(loader, true);
    }
    
    requiredValues(loader) {
        if (!(this._contentType & ALLOWS_ITEMS))
            throw errors.unexpectedFieldContent(this._context, null, this, 'expectedItems');
        
        return this._elements.map(item => {
            item.touch();
            return item.requiredValue(loader);
        });
    }
    
    /**
    * Returns a debug representation of this {@link Field} in one of the respective forms:
    * - `[object Field key=foo attributes=2]`
    * - `[object Field key=foo items=2]`
    * - `[object Field key=foo value=bar]`
    *
    * @return {string} A debug representation of this {@link Field}.
    */
    toString() {
        if ((this._contentType & HAS_ATTRIBUTES) === HAS_ATTRIBUTES)
            return `[object Field key=${this._key} attributes=${this._elements.length}]`;
        if ((this._contentType & HAS_ITEMS) === HAS_ITEMS)
            return `[object Field key=${this._key} items=${this._elements.length}]`;
        if (this._value !== null)
            return `[object Field key=${this._key} value=${this._printValue()}]`;
        
        return `[object Field key=${this._key}]`;
    }
    
    touch(recursive = false) {
        this._touched = true;
        
        if (recursive && (this._contentType & (HAS_ATTRIBUTES | HAS_ITEMS))) {
            for (const element of this._elements) {
                element.touch();
            }
        }
    }
}
