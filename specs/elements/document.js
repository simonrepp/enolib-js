import { parse } from 'enolib';

describe('Document', () => {
    let document;
    
    beforeEach(() => {
        document = parse('');
    });
    
    describe('elements()', () => {
        let result;
        
        beforeEach(() => {
            result = document.elements();
        });
        
        it('returns the elements of the document', () => {
            expect(result).toEqual([]);
        });
    });
    
    describe('toString()', () => {
        it('returns a debug abstraction', () => {
            expect(section.toString()).toEqual('[object Document elements=0]');
        });
    });
    
    describe('toStringTag symbol', () => {
        it('returns a custom tag', () => {
            expect(Object.prototype.toString.call(section)).toEqual('[object Document]');
        });
    });
});
