import { ValidationError } from '../error_types.js';
import { cursor, selection, selectElement, selectKey } from './selections.js';
import { RANGE_BEGIN, RANGE_END } from '../constants.js';

// TODO: Here and prominently also elsewhere - consider replacing instruction._ranges.line with instruction[LINE_RANGE] (where LINE_RANGE = Symbol('descriptive'))

export const errors = {
    elementError: (context, message, element) => {
        return new ValidationError(
            message,
            new context.reporter(context).reportElement(element).snippet(),
            selectElement(element)
        );
    },
    keyError: (context, message, element) => {
        return new ValidationError(
            context.messages.keyError(message),
            new context.reporter(context).reportLine(element).snippet(),
            selectKey(element)
        );
    },
    missingElement: (context, key, parent, message) => {
        return new ValidationError(
            key === null ? context.messages[message] : context.messages[message + 'WithKey'](key),
            new context.reporter(context).reportMissingElement(parent).snippet(),
            selection(parent, 'line', RANGE_END)
        );
    },
    missingValue: (context, element) => {
        let message;
        const selection = {};
        
        if (element.isEmbed() || element.isField()) {
            message = context.messages.missingFieldValue(element._key); // TODO: Differentiate between missingEmbedValue and missingFieldValue!
            
            if (element.isField()) {
                selection.from = cursor(element, 'fieldOperator', RANGE_END);
            } else {
                selection.from = cursor(element, 'line', RANGE_END);
            }
        } else if (element.isAttribute()) {
            message = context.messages.missingAttributeValue(element._key);
            selection.from = cursor(element, 'attributeOperator', RANGE_END);
        } else if (element.isItem()) {
            message = context.messages.missingItemValue(element._parent._key);
            selection.from = cursor(element, 'itemOperator', RANGE_END);
        }
        
        const snippet = new context.reporter(context).reportElement(element).snippet();
        
        selection.to = cursor(element, 'line', RANGE_END);
        
        return new ValidationError(message, snippet, selection);
    },
    unexpectedElement: (context, message, element) => {
        return new ValidationError(
            message || context.messages.unexpectedElement,
            new context.reporter(context).reportElement(element).snippet(),
            selectElement(element)
        );
    },
    unexpectedFieldContent: (context, key, field, message) => {
        return new ValidationError(
            key === null ? context.messages[message] : context.messages[message + 'WithKey'](key),
            new context.reporter(context).reportElement(field).snippet(),
            selectElement(field)
        );
    },
    unexpectedMultipleElements: (context, key, elements, message) => {
        return new ValidationError(
            key === null ? context.messages[message] : context.messages[message + 'WithKey'](key),
            new context.reporter(context).reportElements(elements).snippet(),
            selectElement(elements[0])
        );
    },
    unexpectedElementType: (context, key, section, message) => {
        return new ValidationError(
            key === null ? context.messages[message] : context.messages[message + 'WithKey'](key),
            new context.reporter(context).reportElement(section).snippet(),
            selectElement(section)
        );
    },
    valueError: (context, message, element) => {
        let snippet, select;
        
        if (element.isEmbed()) {
            if (element._lines.length > 0) {
                snippet = new context.reporter(context).reportEmbedValue(element).snippet();
                select = selection(element._lines[0], 'line', RANGE_BEGIN, element._lines[element._lines.length - 1], 'line', RANGE_END);
            } else {
                snippet = new context.reporter(context).reportElement(element).snippet();
                select = selection(element, 'line', RANGE_END);
            }
        } else {
            snippet = new context.reporter(context).reportElement(element).snippet();
            select = {};
            
            if (Object.hasOwn(element._ranges, 'value')) {
                select.from = cursor(element, 'value', RANGE_BEGIN);
            } else if (element.isField()) {
                select.from = cursor(element, 'fieldOperator', RANGE_END);
            } else if (element.isAttribute()) {
                select.from = cursor(element, 'attributeOperator', RANGE_END);
            } else /* if (element.isItem()) */ {
                select.from = cursor(element, 'itemOperator', RANGE_END);
            }
            
            if (Object.hasOwn(element._ranges, 'value')) {
                select.to = cursor(element, 'value', RANGE_END);
            } else {
                select.to = cursor(element, 'line', RANGE_END);
            }
        }
        
        return new ValidationError(context.messages.valueError(message), snippet, select);
    }
};
