export default {
    collectCoverageFrom: ['lib/**'],
    testRegex: '/specs/.*\\.spec\\.js$',
    transform: {}
}
