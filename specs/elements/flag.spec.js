import { parse } from 'enolib';

describe('Flag', () => {
    let flag;
    
    beforeEach(() => {
        flag = parse('flag').flag('flag');
    });
    
    it('is untouched after initialization', () => {
        expect(parse('flag')._elements[0]._touched).toBe(false);
    });
    
    describe('snippet()', () => {
        it('returns the right snippet', () => {
            expect(flag.snippet()).toMatchSnapshot();
        });
    });
    
    describe('toString()', () => {
        it('returns a debug abstraction', () => {
            expect(flag.toString()).toEqual('[object Flag key=flag]');
        });
    });
    
    describe('toStringTag symbol', () => {
        it('returns a custom tag', () => {
            expect(Object.prototype.toString.call(flag)).toEqual('[object Flag]');
        });
    });
});
