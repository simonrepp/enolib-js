import { unpackInternal } from './util.js';

const input = `
field:

- value

-    value

    - value

    -    value

- value
`.trim();

describe('Item tokenization', () => {
    it('performs as specified', () => {
        expect(unpackInternal(input)).toMatchSnapshot();
    });
});
