// THIS FILE IS AUTO-GENERATED (Please submit permanent changes to https://codeberg.org/simonrepp/enolib-common)

import { parse, ParseError, ValidationError } from 'enolib';

describe('Querying an existing required string value from an embed', () => {
    it('produces the expected result', () => {
        const input = `-- embed\n` +
                      `value\n` +
                      `-- embed`;
        
        const output = parse(input).embed('embed').requiredValue();
        
        const expected = `value`;
        
        expect(output).toEqual(expected);
    });
});

describe('Querying an existing optional string value from an embed', () => {
    it('produces the expected result', () => {
        const input = `-- embed\n` +
                      `value\n` +
                      `-- embed`;
        
        const output = parse(input).embed('embed').optionalValue();
        
        const expected = `value`;
        
        expect(output).toEqual(expected);
    });
});

describe('Querying a missing optional string value from an embed', () => {
    it('produces the expected result', () => {
        const input = `-- embed\n` +
                      `-- embed`;
        
        const output = parse(input).embed('embed').optionalValue();
        
        expect(output).toBeNull();
    });
});