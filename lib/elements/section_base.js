import { MissingEmbed} from './missing/missing_embed.js';
import { MissingField} from './missing/missing_field.js';
import { MissingFlag } from './missing/missing_flag.js';
import { MissingSectionElement } from './missing/missing_section_element.js';
import { MissingSection } from './missing/missing_section.js';
import { errors } from '../errors/validation.js';
import { ElementBase } from './element_base.js';

export class SectionBase extends ElementBase {
    _element(key, required = null) {
        const elements = key === null ? this._elements : this._elementsMap(key);
        
        if (elements.length === 0) {
            if (required || required === null && this._allElementsRequired) {
                throw errors.missingElement(this._context, key, this, 'missingElement');
            } else if (required === null) {
                return new MissingSectionElement(key, this);
            } else {
                return null;
            }
        }
        
        if (elements.length > 1)
            throw errors.unexpectedMultipleElements(
                this._context,
                key,
                elements,
                'expectedSingleElement'
            );

        elements[0].touch(); // TODO: Here and elsewhere: touch is always recursive, but here in this case shouldn't be!
        
        return elements[0];
    }
    
    _elementsMap(key = null) {
        if (!Object.hasOwn(this, '_elementsMapCache')) {
            this._elementsMapCache = {};
            for (const element of this._elements) {
                if (Object.hasOwn(this._elementsMapCache, element._key)) {
                    this._elementsMapCache[element._key].push(element);
                } else {
                    this._elementsMapCache[element._key] = [element];
                }
            }
        }

        if (key === null) return this._elementsMapCache
        if (Object.hasOwn(this._elementsMapCache, key)) return this._elementsMapCache[key];
        return [];
    }
    
    _embed(key, required = null) {
        const elements = key === null ? this._elements : this._elementsMap(key);
        
        if (elements.length === 0) {
            if (required || required === null && this._allElementsRequired) {
                throw errors.missingElement(this._context, key, this, 'missingEmbed');
            } else if (required === null) {
                return new MissingEmbed(key, this);
            } else {
                return null;
            }
        }
        
        if (elements.length > 1)
            throw errors.unexpectedMultipleElements(
                this._context,
                key,
                elements,
                'expectedSingleEmbed'
            );
        
        const element = elements[0];
        
        if (!element.isEmbed())
            throw errors.unexpectedElementType(this._context, key, element, 'expectedEmbed');
        
        element.touch();

        return element;
    }
    
    _flag(key, required = null) {
        const elements = key === null ? this._elements : this._elementsMap(key);
        
        if (elements.length === 0) {
            if (required || required === null && this._allElementsRequired) {
                throw errors.missingElement(this._context, key, this, 'missingFlag');
            } else if (required === null) {
                return new MissingFlag(key, this);
            } else {
                return null;
            }
        }
        
        if (elements.length > 1)
            throw errors.unexpectedMultipleElements(
                this._context,
                key,
                elements,
                'expectedSingleFlag'
            );
        
        const element = elements[0];
        
        if (!element.isFlag())
            throw errors.unexpectedElementType(this._context, key, element, 'expectedFlag');
        
        element.touch();

        return element;
    }
    
    _field(key, required = null) {
        const elements = key === null ? this._elements : this._elementsMap(key);

        if (elements.length === 0) {
            if (required || required === null && this._allElementsRequired) {
                throw errors.missingElement(this._context, key, this, 'missingField');
            } else if (required === null) {
                return new MissingField(key, this);
            } else {
                return null;
            }
        }
        
        if (elements.length > 1)
            throw errors.unexpectedMultipleElements(
                this._context,
                key,
                elements,
                'expectedSingleField'
            );
        
        const element = elements[0];
        
        if (!element.isField())
            throw errors.unexpectedElementType(this._context, key, element, 'expectedField');

        element.touch();

        return element;
    }
    
    // TODO: Can probably be simplified again - e.g. pushed back into Missing* classes themselves - also check if MissingAttribute addition is made use of already
    _missingError(element) {
        if (element instanceof MissingEmbed) {
            throw errors.missingElement(this._context, element._key, this, 'missingEmbed');
        } else if (element instanceof MissingField) {
            throw errors.missingElement(this._context, element._key, this, 'missingField');
        } else if (element instanceof MissingFlag) {
            throw errors.missingElement(this._context, element._key, this, 'missingFlag');
        } else if (element instanceof MissingSection) {
            throw errors.missingElement(this._context, element._key, this, 'missingSection');
        } else {
            throw errors.missingElement(this._context, element._key, this, 'missingElement');
        }
    }
    
    _section(key, required = null) {
        const elements = key === null ? this._elements : this._elementsMap(key);
        
        if (elements.length === 0) {
            if (required || required === null && this._allElementsRequired) {
                throw errors.missingElement(this._context, key, this, 'missingSection');
            } else if (required === null) {
                return new MissingSection(key, this);
            } else {
                return null;
            }
        }
        
        if (elements.length > 1)
            throw errors.unexpectedMultipleElements(
                this._context,
                key,
                elements,
                'expectedSingleSection'
            );
        
        const element = elements[0];
        
        if (!element.isSection())
            throw errors.unexpectedElementType(this._context, key, element, 'expectedSection');

        element.touch();

        return element;
    }
    
    _untouched() {
        if (!this._touched)
            return this;
        
        for (const element of this._elements) {
            const untouchedElement = element._untouched();
            
            if (untouchedElement) return untouchedElement;
        }
        
        return false;
    }
    
    allElementsRequired(required = true) {
        this._allElementsRequired = required;
        
        for (const element of this._elements) {
            if (element.isField()) {
                element.allAttributesRequired(required);
            } else if (element.isSection()) {
                element.allElementsRequired(required);
            }
        }
    }
    
    /**
    * Assert that all elements inside this section/document have been touched
    * @param {string} message A static string error message or a message function taking the excess element and returning an error string
    * @param {object} options
    * @param {array} options.except An array of element keys to exclude from assertion
    * @param {array} options.only Specifies to ignore all elements but the ones includes in this array of element keys
    */
    assertAllTouched(...optional) {
        let message = null;
        let options = {};
        
        for (const argument of optional) {
            if (typeof argument === 'object') {
                options = argument;
            } else {
                message = argument
            }
        }
        
        for (const [key, elements] of Object.entries(this._elementsMap())) {
            if (Object.hasOwn(options, 'except') && options.except.includes(key)) continue;
            if (Object.hasOwn(options, 'only') && !options.only.includes(key)) continue;
            
            for (const element of elements) {
                const untouched = element._untouched();
                if (untouched) {
                    if (typeof message === 'function') { message = message(untouched); }
                    throw errors.unexpectedElement(this._context, message, untouched);
                }
            }
        }
    }
    
    element(key = null) {
        return this._element(key);
    }
    
    /**
    * Returns the elements of this {@link Section} as an array in the original document order.
    *
    * @param {string} [key] If provided only elements with the specified key are returned.
    * @return {Element[]} The elements of this {@link Section}.
    */
    elements(key = null) {
        const elements = key === null ? this._elements : this._elementsMap(key);
        for (const element of elements) { element.touch(); }
        return elements;
    }
    
    // TODO: Here and in other implementations and in missing_section: flags(...) ?
    
    embed(key = null) {
        return this._embed(key);
    }
    
    embeds(key = null) {
        const elements = key === null ? this._elements : this._elementsMap(key);

        for (const element of elements) {
            if (!element.isEmbed())
                throw errors.unexpectedElementType(this._context, key, element, 'expectedEmbeds');
        }

        for (const element of elements) { element.touch(); }
        
        return elements;
    }
    
    field(key = null) {
        return this._field(key);
    }
    
    fields(key = null) {
        const elements = key === null ? this._elements : this._elementsMap(key);

        for (const element of elements) {
            if (!element.isField())
                throw errors.unexpectedElementType(this._context, key, element, 'expectedFields');
        }

        for (const element of elements) { element.touch(); }
        
        return elements;
    }
    
    flag(key = null) {
        return this._flag(key);
    }
    
    flags(key = null) {
        const elements = key === null ? this._elements : this._elementsMap(key);

        for (const element of elements) {
            if (!element.isFlag())
                throw errors.unexpectedElementType(this._context, key, element, 'expectedFlags');
        }

        for (const element of elements) { element.touch(); }
        
        return elements;
    }
    
    optionalElement(key = null) {
        return this._element(key, false);
    }
    
    optionalEmbed(key = null) {
        return this._embed(key, false);
    }
    
    optionalField(key = null) {
        return this._field(key, false);
    }
    
    optionalFlag(key = null) {
        return this._flag(key, false);
    }
    
    optionalSection(key = null) {
        return this._section(key, false);
    }
    
    requiredElement(key = null) {
        return this._element(key, true);
    }
    
    requiredEmbed(key = null) {
        return this._embed(key, true);
    }
    
    requiredField(key = null) {
        return this._field(key, true);
    }
    
    requiredFlag(key = null) {
        return this._flag(key, true);
    }
    
    requiredSection(key = null) {
        return this._section(key, true);
    }
    
    section(key = null) {
        return this._section(key);
    }
    
    sections(key = null) {
        const elements = key === null ? this._elements : this._elementsMap(key);

        for (const element of elements) {
            if (!element.isSection())
                throw errors.unexpectedElementType(this._context, key, element, 'expectedSections');
        }

        for (const element of elements) { element.touch(); }
        
        return elements;
    }
}