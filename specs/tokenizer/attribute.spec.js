import { unpackInternal } from './util.js';

const input = `
field:

attribute = value

attribute    = value

    attribute = value

    attribute    = value

    attribute    =    value

attribute = value
`.trim();

describe('Attribute tokenization', () => {
    it('performs as specified', () => {
        expect(unpackInternal(input)).toMatchSnapshot();
    });
});
