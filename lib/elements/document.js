import { lookup } from '../lookup.js';
import { SectionBase } from './section_base.js';

export class Document extends SectionBase {
    constructor(context) {
        const line = 0;
        const ranges = { line: [0, 0] };

        super(context, line, ranges);

        this._allElementsRequired = false;
        this._elements = [];
    }
    
    get [Symbol.toStringTag]() {
        return 'Document';
    }

    isDocument() {
        return true;
    }

    /**
     * Return the element that is located at the specified position in
     * the document. The position can either be a zero-indexed one-dimensional
     * index into the eno string that was given for parsing, or a combination
     * of line and column, but in both cases passed as an object.
     * @param {object} position Either of the form { index: 3 } or { line: 0 , column: 3 }
     * @return {object} Of the form { element: ..., range: ... } where "range" is the sub-part of an element (e.g. key, operator, value)
     */
    lookup(position) {
        return lookup(position, this._context);
    }
    
    /**
    * Returns null.
    *
    * @return null.
    */
    parent() {
        return null;
    }
    
    /**
    * Returns a debug representation of this {@link Document} in the form of `[object Document elements=2]`.
    *
    * @return {string} A debug representation of this {@link Document}.
    */
    toString() {
        return `[object Document elements=${this._elements.length}]`;
    }
}