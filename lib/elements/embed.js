import { RANGE_BEGIN, RANGE_END } from '../constants.js';
import { ValueElementBase } from './value_element_base.js';

export class Embed extends ValueElementBase {
    constructor(
        context,
        key,
        line,
        lines,
        parent,
        ranges,
        terminator
    ) {
        super(context, line, ranges);

        this._key = key;
        this._lines = lines;
        this._parent = parent;
        this._terminator = terminator;
        this._touched = false;
    }
    
    _getValue() {
        if (this._lines.length === 0) return null;

        return this._context._input.substring(
            this._lines[0]._ranges.line[RANGE_BEGIN],
            this._lines[this._lines.length - 1]._ranges.line[RANGE_END]
        );
    }

    get [Symbol.toStringTag]() {
        return 'Embed';
    }

    /*
     * Query whether this element is an embed
     */
    isEmbed() {
        return true;
    }
    
    // TODO: This could be implemented generically on whatever Embed inherits from in the end (here and elsewhere)
    /**
    * Returns the parent {@link Section}.
    *
    * @return {Section} The parent section.
    */
    parent() {
        return this._parent;
    }
    
    /**
    * Returns a debug representation of this {@link Embed} in the form of `[object Embed key=foo value=bar]`
    *
    * @return {string} A debug representation of this {@link Embed}.
    */
    toString() {
        if (this._lines.length)
            return `[object Embed key=${this._key} value=${this._printValue()}]`;
        
        return `[object Embed key=${this._key}]`;
    }
}
