import messages from '../lib/messages.js';

describe('Messages', () => {
    for (const [message, translation] of Object.entries(messages)) {
        describe(message, () => {
            it('contains a static string translation or a message generator function', () => {
                if(typeof translation === 'function') {
                    const generatedMessage = translation();
                    expect(typeof generatedMessage).toEqual('string');
                } else {
                    expect(typeof translation).toEqual('string');
                }
            });
        });
    }
});
