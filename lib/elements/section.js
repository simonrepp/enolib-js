import { SectionBase } from './section_base.js';

export class Section extends SectionBase {
    constructor(
        context,
        key,
        line,
        parent,
        ranges
    ) {
        super(context, line, ranges);
        
        this._allElementsRequired = false;
        this._elements = [];
        this._key = key;
        this._parent = parent;
        this._touched = false;
    }
    
    get [Symbol.toStringTag]() {
        return 'Section';
    }
    
    isSection() {
        return true;
    }
    
    /**
    * Returns the parent {@link Section} or {@link Document}.
    *
    * @return {?Section} The parent section or document.
    */
    parent() {
        return this._parent;
    }
    
    /**
    * Returns a debug representation of this {@link Section} in the form of `[object Section key=foo elements=2]`.
    *
    * @return {string} A debug representation of this {@link Section}.
    */
    toString() {
        return `[object Section key=${this._key} elements=${this._elements.length}]`;
    }
    
    touch(recursive = false) {
        this._touched = true;
        if (recursive) {
            for (const element of this._elements) {
                element.touch();
            }
        }
    }
}