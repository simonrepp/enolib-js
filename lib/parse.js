import { Context } from './context.js';
import { Parser } from './parser.js';

/**
 * Main parser entry point
 * @param {string} input The *content* of an eno document as a string
 * @param {object} options Optional parser settings
 * @param {string} options.source A source label to include in error messages - provide (e.g.) a filename or path to let users know in which file the error occured.
 */
export function parse(input, options = {}) {
    const context = new Context(input, options);
    const parser = new Parser(context);

    return parser.parse();
}
