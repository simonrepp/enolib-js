import { Document } from './elements/document.js';
import messages from './messages.js';
import { TextReporter } from './reporters/text_reporter.js';

export class Context {
    constructor(input, options) {
        this._document = new Document(this);
        this._input = input;
        this.messages = Object.hasOwn(options, 'locale') ? options.locale : messages;
        this._meta = [];
        this.reporter = Object.hasOwn(options, 'reporter') ? options.reporter : TextReporter;
        this.source = Object.hasOwn(options, 'source') ? options.source : null;
    }
}
