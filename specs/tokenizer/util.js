import { parse } from 'enolib';

function attachUnpackedSectionElements(documentOrSectionUnpacked, documentOrSection) {
    const elements = documentOrSection.elements();
    
    if (elements.length === 0)
        return;

    documentOrSectionUnpacked.elements = elements.map(element => {
        if (element.isSection())
            return unpackSection(element);
        
        const elementUnpacked = {
            _line: element._line,
            _ranges: element._ranges,
            key: element.key()
        };
            
        if (element.isEmbed()) {
            elementUnpacked.type = 'embed';
            elementUnpacked._terminator = element._terminator;
            
            unpackValue(element, elementUnpacked);
        } else if (element.isField()) {
            elementUnpacked.type = 'field';
            
            if (element.hasAttributes()) {
                elementUnpacked.attributes = element.attributes().map(attribute => {
                    const attributeUnpacked = {
                        _line: attribute._line,
                        _ranges: attribute._ranges,
                        key: attribute.key()
                    };
                    
                    unpackValue(attribute, attributeUnpacked);
                    
                    return attributeUnpacked;
                });
            } else if (element.hasItems()) {
                elementUnpacked.items = element.items().map(item => {
                    const itemUnpacked = {
                        _line: item._line,
                        _ranges: item._ranges,
                    };
                    
                    unpackValue(item, itemUnpacked);
                    
                    return itemUnpacked;
                });
            } else if (element.hasValue()) {
                unpackValue(element, elementUnpacked);
            }
        } else /* if (element.isFlag()) */ {
            elementUnpacked.type = 'flag';
        }
        
        return elementUnpacked;
    });
}

export function unpackInternal(input) {
    const document = parse(input);

    const documentUnpacked = {
        _line: document._line,
        _meta: [],
        _ranges: document._ranges,
        type: 'document'
    };

    for (const meta of document._context._meta) {
        documentUnpacked._meta.push(meta);
    }

    attachUnpackedSectionElements(documentUnpacked, document)
    
    return documentUnpacked;
}


function unpackSection(section) {
    const sectionUnpacked = {
        key: section.key(),
        _line: section._line,
        _ranges: section._ranges,
        type: 'section'
    };
    
    attachUnpackedSectionElements(sectionUnpacked, section)
    
    return sectionUnpacked;
}

function unpackValue(element, target) {
    const value = element.optionalValue();

    if (value) {
        target.value = value;
    }
}