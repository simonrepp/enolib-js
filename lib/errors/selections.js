import { RANGE_BEGIN, RANGE_END } from '../constants.js';

function lastIn(element) {
    if (element.isField()) {
        if (element.hasAttributes()) {
            return lastIn(element._elements[element._elements.length - 1]);
        } else if (element.hasItems()) {
            return lastIn(element._elements[element._elements.length - 1]);
        }
    } else if (element.isEmbed()) {
        return element._terminator;
    } else if (element.isSection() && element._elements.length > 0) {
        return lastIn(element._elements[element._elements.length - 1]);
    }
    
    return element;
}

export function cursor(instruction, range, position) {
    const index = instruction._ranges[range][position];
    
    return {
        column: index - instruction._ranges.line[RANGE_BEGIN],
        index: index,
        line: instruction._line
    };
}

export function selection(instruction, range, position, ...to) {
    const toInstruction = to.find(argument => typeof argument === 'object') || instruction;
    const toRange = to.find(argument => typeof argument === 'string') || range;
    const toPosition = to.find(argument => typeof argument === 'number') || position;
    
    return {
        from: cursor(instruction, range, position),
        to: cursor(toInstruction, toRange, toPosition)
    };
}

export const selectElement = element => selection(element, 'line', RANGE_BEGIN, lastIn(element), 'line', RANGE_END);
export const selectKey = element => selection(element, 'key', RANGE_BEGIN, RANGE_END);
export const selectLine = element => selection(element, 'line', RANGE_BEGIN, RANGE_END);
