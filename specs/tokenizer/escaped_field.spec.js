import { unpackInternal } from './util.js';

const input = `
\`key\`:

\`\`ke\`y\`\`:

\`\`\`k\`\`ey\`\`\`    :

    \`\` \`key\` \`\`    :

\`key\`:
`.trim();

describe('Escaped key tokenization', () => {
    it('performs as specified', () => {
        expect(unpackInternal(input)).toMatchSnapshot();
    });
});
