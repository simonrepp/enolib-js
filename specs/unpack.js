// TODO: Add additional data points to unpacking - line, line ranges etc. and expand public API for this if not available yet

function attachUnpackedSectionElements(documentOrSectionUnpacked, documentOrSection) {
    const elements = documentOrSection.elements();
    
    if (elements.length === 0)
        return;

    documentOrSectionUnpacked.elements = elements.map(element => {
        if (element.isSection())
            return unpackSection(element);
        
        const elementUnpacked = { key: element.key() };
            
        if (element.isEmbed()) {
            elementUnpacked.type = 'embed';
            
            unpackValue(element, elementUnpacked);
        } else if (element.isField()) {
            elementUnpacked.type = 'field';
            
            if (element.hasAttributes()) {
                elementUnpacked.attributes = element.attributes().map(attribute => {
                    const attributeUnpacked = { key: attribute.key() };
                    
                    unpackValue(attribute, attributeUnpacked);
                    
                    return attributeUnpacked;
                });
            } else if (element.hasItems()) {
                elementUnpacked.items = element.items().map(item => {
                    const itemUnpacked = {};
                    
                    unpackValue(item, itemUnpacked);
                    
                    return itemUnpacked;
                });
            } else if (element.hasValue()) {
                unpackValue(element, elementUnpacked);
            }
        } else /* if (element.isFlag()) */ {
            elementUnpacked.type = 'flag';
        }
        
        return elementUnpacked;
    });
}

export function unpack(document) {
    const documentUnpacked = { type: 'document' };
    attachUnpackedSectionElements(documentUnpacked, document)
    return documentUnpacked;
}


function unpackSection(section) {
    const sectionUnpacked = {
        key: section.key(),
        type: 'section'
    };
    
    attachUnpackedSectionElements(sectionUnpacked, section)
    
    return sectionUnpacked;
}

function unpackValue(element, target) {
    const value = element.optionalValue();

    if (value) {
        target.value = value;
    }
}