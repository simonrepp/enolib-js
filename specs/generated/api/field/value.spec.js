// THIS FILE IS AUTO-GENERATED (Please submit permanent changes to https://codeberg.org/simonrepp/enolib-common)

import { parse, ParseError, ValidationError } from 'enolib';

describe('Querying an existing required string value from a field', () => {
    it('produces the expected result', () => {
        const input = `field: value`;
        
        const output = parse(input).field('field').requiredValue();
        
        const expected = `value`;
        
        expect(output).toEqual(expected);
    });
});

describe('Querying an existing optional string value from a field', () => {
    it('produces the expected result', () => {
        const input = `field: value`;
        
        const output = parse(input).field('field').optionalValue();
        
        const expected = `value`;
        
        expect(output).toEqual(expected);
    });
});

describe('Querying a missing optional string value from a field', () => {
    it('produces the expected result', () => {
        const input = `field:`;
        
        const output = parse(input).field('field').optionalValue();
        
        expect(output).toBeNull();
    });
});