import { ValueElementBase } from './value_element_base.js';

export class Attribute extends ValueElementBase {
    constructor(
        context,
        key,
        line,
        parent,
        ranges,
        value
    ) {
        super(context, line, ranges);

        this._key = key;
        this._parent = parent;
        this._touched = false;
        this._value = value;
    }

    get [Symbol.toStringTag]() {
        return 'Attribute';
    }

    isAttribute() {
        return true;
    }
    
    parent() {
        return this._parent;
    }
    
    toString() {
        return `[object Attribute key=${this._key} value=${this._printValue()}]`;
    }
}
