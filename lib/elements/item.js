import { ValueElementBase } from './value_element_base.js';

export class Item extends ValueElementBase {
    constructor(
        context,
        line,
        parent,
        ranges,
        value
    ) {
        super(context, line, ranges);

        this._parent = parent;
        this._touched = false;
        this._value = value;
    }

    get [Symbol.toStringTag]() {
        return 'Item';
    }

    get _key() {
        return this._parent._key;
    }

    /**
    * Check whether the item has a value.
    *
    * @return {bool} Whether the item has a value.
    */
    hasValue() {
        return this._value !== null;
    }

    isItem() {
        return true;
    }
    
    parent() {
        return this._parent;
    }
    
    toString() {
        return `[object Item value=${this._printValue()}]`;
    }
}
