import { MissingElementBase } from './missing_element_base.js';

export class MissingValueElementBase extends MissingElementBase {
    optionalValue(_loader) {
        return null;
    }
    
    requiredValue() {
        this._parent._missingError(this);
    }
}