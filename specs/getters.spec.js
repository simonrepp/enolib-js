import { parse } from 'enolib';

const sample = `
field: value
items:
- item
- item
attributes:
foo = bar
bar = baz
empty:
`.trim();

const expected = {
    attributes: {
        foo: 'bar',
        bar: 'baz'
    },
    empty: [],
    field: 'value',
    items: [
        'item',
        'item'
    ]
};

describe('Getters', () => {
    test('return values as expected', () => {
        const document = parse(sample);
        
        const result = {
            attributes: document.field('attributes'),
            empty: document.field('empty').requiredValues(),
            field: document.field('field').requiredValue(),
            items: document.field('items').requiredValues()
        };
        
        result.attributes  = {
            foo: result.attributes.attribute('foo').requiredValue(),
            bar: result.attributes.attribute('bar').requiredValue()
        };
        
        expect(result).toEqual(expected);
    });
});
