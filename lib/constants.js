// Added to 0-indexed indices in a few places
export const HUMAN_INDEXING = 1;

// More semantic access to ranges
export const RANGE_BEGIN = 0;
export const RANGE_END = 1;