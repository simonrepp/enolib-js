import { parse } from 'enolib';

describe('Section', () => {
    let section;
    
    beforeEach(() => {
        section = parse('# section').section('section');
    });

    it('is untouched after initialization', () => {
        expect(parse('# section')._elements[0]._touched).toBe(false);
    });
    
    describe('elements()', () => {
        let result;
        
        beforeEach(() => {
            result = section.elements();
        });
        
        it('returns the elements of the section', () => {
            expect(result).toEqual([]);
        });
    });
    
    describe('toString()', () => {
        it('returns a debug abstraction', () => {
            expect(section.toString()).toEqual('[object Section key=section elements=0]');
        });
    });
    
    describe('toStringTag symbol', () => {
        it('returns a custom tag', () => {
            expect(Object.prototype.toString.call(section)).toEqual('[object Section]');
        });
    });
});
